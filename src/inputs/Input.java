package inputs;

/**
 * @author January, 27, 2020 by Shahin Montazeri {shahin.montazeri@gmail.com}
 * @version 1
 * Class providing input values for practices executed in output
 * @see outputs.Output
 */
public class Input {

    /**
     * inputs for problem 1
     * @return
     */
    public static String problem_1(){
        return "Ab,c,de!$";
    }

    /**
     * inputs for problem 2
     * @return
     */
    public static int[] problem_2_powers(){
        return new int[]{
                -4,
                3,
                5,
                -2,
                1
        };
    }

    /**
     * inputs for problem 3
     * @return
     */
    public static int[] problem_3(){
        return new int[]{
                1,
                3,
                4,
                6
        };
    }


}
