package outputs;

import inputs.Input;

import java.util.*;

/**
 * @author January, 27, 2020 by Shahin Montazeri {shahin.montazeri@gmail.com}
 * @version 1
 * for assumed input values refer to the class
 * @see Input
 */
public class Output {
    public static void main(String[] args) {

        //region Solution to Problem 1
        System.out.println("Problem 1 - Reverse All Practice : " + reverseAll(Input.problem_1()));
        System.out.println("Problem 1 - Solution Result : " + reverseSolution(Input.problem_1()));
        //endregion

        //region Solution to Problem 2
        for (int i = 0; i < Input.problem_2_powers().length; i++) {
            System.out.println("Problem 2 - Solution Result : " + "OPERATION " + i + " ^ " + Input.problem_2_powers()[i] + " | OUTPUT = " + powerComputation(i, Input.problem_2_powers()[i]));
        }
        //endregion

        //region Solution to Problem 3
        System.out.println("Problem 3 - Solution Result : " + equation(Input.problem_3()));
        //endregion

        //region Solution to Problem 4
        for (int i = 0; i <= 10; i++) {
            System.out.println("Problem 4 - Solution Result : " + "AVERAGE FOR [1," + i + "] | OUTPUT = " + findAverage(new int[]{1, i}));
        }
        //endregion

    }

    /**
     * practice for problem 1
     * @see Input for String input
     * @param input
     * @return
     */
    private static String reverseAll(String input) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(input);
        stringBuilder = stringBuilder.reverse();
        return stringBuilder.toString();
    }

    /**
     * solution for problem 1
     * @see Input for String input
     * @param input
     * @return
     */
    private static String reverseSolution(String input) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] chars = input.toCharArray();
        List<HashMap<Character, Integer>> specialCharacterList = new ArrayList<>();
        for (int i = 0; i < chars.length; i++){
            HashMap<Character, Integer> hashMap = new HashMap<>();
            hashMap.put(chars[i], i);
            if (!Character.isAlphabetic(chars[i])) {
                specialCharacterList.add(hashMap);
            } else {
                stringBuilder.append(chars[i]);
            }
        }
        stringBuilder = stringBuilder.reverse();

        for (int i = 0; i < specialCharacterList.size(); i++) {
            stringBuilder.insert(specialCharacterList.get(i).get(specialCharacterList.get(i).keySet().toArray()[0]), specialCharacterList.get(i).keySet().toArray()[0]);
        }
        return stringBuilder.toString();
    }

    /**
     * solution for problem 2
     * @see Input for inputs
     * @param base is assumed as an integer from a for loop index position
     * @param power
     * @return
     */
    private static Double powerComputation(Integer base, Integer power) {
        while (power > 0) {
            return Math.pow(base, power);
        } return null;
    }

    /**
     * the equation is 6÷(1–3/4)=24 yet the possibility of other variations will be researched
     * the function should work for any arrangement of Integer set of [1,3,4,6]. such as [4,3,1,6]
     * @see Input for input integers
     * @param args
     * @return
     */
    private static Integer equation(int[] args) {
        final int expectedResult = 24;

        int value_a = 0; // projected 1
        int value_b = 0; // projected 3
        int value_c = 0; // projected 4
        int value_d = 0; // projected 6

        int argsSize = args.length;
        List<Integer> leftOvers = new ArrayList<>();
        for (int i = 0; i < argsSize; i++) {
            if (24 % args[i] == 0) {
                if (args[i] != 1){
                    leftOvers.add(args[i]);
                } else {
                    value_a = args[i]; // possible 1
                }
            }
        }
        value_b = Collections.min(leftOvers); // possible 3
        value_d = Collections.max(leftOvers); // possible 6
        for (int integer : leftOvers) {
            if (integer != value_b && integer != value_d) {
                value_c = integer; // possible 4
            }
        }

        System.out.println("Value A = " + value_a);
        System.out.println("Value B = " + value_b);
        System.out.println("Value C = " + value_c);
        System.out.println("Value D = " + value_d);


        int outcome = (int) (value_d / (value_a - ((double) value_b / value_c)));
        while (outcome == 24) {
            return outcome;
        } return null;
    }

    /**
     * solution for problem 4
     * @param ints is assumed as an integer from a for loop index position
     * @return
     */
    private static Double findAverage(int[] ints) {
        int size = ints.length;
        double sum = 0;
        for (int anInt : ints) {
            sum += anInt;
        }
        return  sum / size;
    }
}
